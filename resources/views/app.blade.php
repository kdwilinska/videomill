<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--bootstrap css-->
    <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!--custom css-->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
    <!--flex slider css-->
    <link href="{{asset('css/flexslider.css')}}" rel="stylesheet">
    <!--google web fonts css-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <!-- icons css-->
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body data-spy="scroll" data-target="#navigation" data-offset="75">

@include('include_menu', array ('page_type' => 'auth'))

<div class="dropdown-menu">

  <!--  <img src="{{asset('img/praca.png')}}" /> -->


    <a href="{{url('/')}}">Home</a>
    <a href="{{url('/templates/create')}}">Create video</a>
    @if (Auth::check())
        <a href="{{url('/my-videos')}}">My videos</a>
    @endif
    <a href="{{url('/templates')}}">Templates</a>

    @if (Auth::check())
        {!! Auth::user()->name !!}
    <a href="{{url('/my-account')}}">My account</a>
    <a href="{{url('/auth/logout')}}">Logout</a>
    @else
    <a href="{{url('/auth/login')}}">Login</a>
    <a href="{{url('/auth/register')}}">Registration</a>
    @endif

    <hr/>


</div>
<div class="container">
    <div class="content">
        @yield('content')

    </div>
</div>
</body>
</html>