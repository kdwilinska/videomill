<h4>{{trans('page.u_account_data_label')}}</h4>
<div class="row">
    <div class="col-md-6">
        <div class="row control-group">
            <div class="form-group col-xs-12 controls">
                {!! Form::label('email',trans('page.u_email').':') !!}<span>*</span>
                {!! Form::text('email',null,['class' => 'form-control','placeholder'=>trans('page.u_email')]) !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row control-group">
            <div class="form-group col-xs-12 controls">
                {!! Form::label('first_name',trans('page.u_first_name').':') !!}<span>*</span>
                {!! Form::text('first_name',null,['class' => 'form-control','placeholder'=>trans('page.u_first_name')]) !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row control-group">
            <div class="form-group col-xs-12 controls">
                {!! Form::label('last_name',trans('page.u_last_name').':') !!}<span>*</span>
                {!! Form::text('last_name',null,['class' => 'form-control','placeholder'=>trans('page.u_last_name')]) !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="row control-group">
            <div class="form-group col-xs-12 controls">
                {!! Form::label('phone',trans('page.u_phone').':') !!}
                {!! Form::text('phone',null,['class' => 'form-control','placeholder'=>trans('page.u_phone')]) !!}
            </div>
        </div>
    </div>
</div>
<h4>{{trans('page.u_company_data_label')}}</h4>
<div class="row">
    <div class="col-md-6">
        <div class="row control-group">
            <div class="form-group col-xs-12 controls">
                {!! Form::label('company_name',trans('page.u_company_name').':') !!}
                {!! Form::text('company_name',null,['class' => 'form-control','placeholder'=>trans('page.u_company_name')]) !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="row control-group">
            <div class="form-group col-xs-12 controls">
                {!! Form::label('company_tax_number',trans('page.u_company_tax_number').':') !!}
                {!! Form::text('company_tax_number',null,['class' => 'form-control','placeholder'=>trans('page.u_company_tax_number')]) !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="row control-group">
            <div class="form-group col-xs-12 controls">
                {!! Form::label('company_country',trans('page.u_company_country').':') !!}
                {!! Form::text('company_country',null,['class' => 'form-control','placeholder'=>trans('page.u_company_country')]) !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row control-group">
            <div class="form-group col-xs-12 controls">
                {!! Form::label('address_line_1',trans('page.u_address_line_1').':') !!}
                {!! Form::text('address_line_1',null,['class' => 'form-control','placeholder'=>trans('page.u_address_line_1')]) !!}
                {!! Form::text('address_line_2',null,['class' => 'form-control','placeholder'=>trans('page.u_address_line_2')]) !!}
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="row control-group">
            <div class="form-group col-xs-12 controls">
                {!! Form::label('address_postal_code',trans('page.u_address_postal_code').':') !!}
                {!! Form::text('address_postal_code',null,['class' => 'form-control','placeholder'=>trans('page.u_address_postal_code')]) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row control-group">
            <div class="form-group col-xs-12 controls">
                {!! Form::label('address_post_office',trans('page.u_address_post_office').':') !!}
                {!! Form::text('address_post_office',null,['class' => 'form-control','placeholder'=>trans('page.u_address_post_office')]) !!}
            </div>
        </div>
    </div>
</div>

<div class="form-group col-xs-12">
    <button type="submit" class="btn btn-theme-color btn-lg">{{$submitButtonText}}</button>
</div>

