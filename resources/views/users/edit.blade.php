@extends('app')
@section('title')
    {{trans('page.u_my_account')}}
@stop
@section('content')

    {!! Form::model($curr_user,['method'=>'POST','action' => ['UsersController@update_curr_user',$locale]]) !!}
        @include('users.form',['submitButtonText'=>trans('page.u_submit_save_user')])
    {!! Form::close() !!}

 @include('errors.list')

@stop