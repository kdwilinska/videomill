        <div class="work-section-1">
            <div class="container">
                <div class="section-heading text-center">
                    <h4 class="small section-title"><span>Latest portfolio</span></h4>
                    <h2 class="large section-title">Our work</h2>
                </div><!--section heading-->
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="portfolio-filters text-center">
                            <li class="filter active" data-filter="all">all</li>
                            <li class="filter" data-filter="design">design</li>
                            <li class="filter" data-filter="html">HTML5</li>
                            <li class="filter" data-filter="wordpress">Wordpress</li>
                            <li class="filter" data-filter="seo">Seo</li>
                        </ul><!--.portfolio-filter nav-->



                        <div id="grid" class="row">
                            <div class="mix col-md-3 col-sm-6 design margin-btm-20">
                                <a href="single-work.html">
                                    <div class="image-wrapper">
                                        <img src="{{asset('img/sec-img1.jpg')}}" class="img-responsive" alt="work-1">
                                        <div class="image-overlay">
                                            <p>
                                                View Detail
                                            </p>
                                        </div><!--.image-overlay-->
                                    </div><!--.image-wrapper-->
                                </a>
                                <div class="work-sesc">

                                    <p>
                                        Web design
                                    </p>
                                </div><!--.work-desc-->
                            </div><!--work item-->
                            <div class="mix col-md-3 col-sm-6 html margin-btm-20">
                                <a href="single-work.html">
                                    <div class="image-wrapper">
                                        <img src="{{asset('img/sec-img2.jpg')}}" class="img-responsive" alt="work-1">
                                        <div class="image-overlay">
                                            <p>
                                                View Detail
                                            </p>
                                        </div><!--.image-overlay-->
                                    </div><!--.image-wrapper-->
                                </a>
                                <div class="work-sesc">

                                    <p>
                                        HTML5
                                    </p>
                                </div><!--.work-desc-->
                            </div><!--work item-->
                            <div class="mix col-md-3 col-sm-6 wordpress design margin-btm-20">
                                <a href="single-work.html">
                                    <div class="image-wrapper">
                                        <img src="{{asset('img/sec-img3.jpg')}}" class="img-responsive" alt="work-1">
                                        <div class="image-overlay">
                                            <p>
                                                View Detail
                                            </p>
                                        </div><!--.image-overlay-->
                                    </div><!--.image-wrapper-->
                                </a>
                                <div class="work-sesc">

                                    <p>
                                        Wordpress/web Design
                                    </p>
                                </div><!--.work-desc-->
                            </div><!--work item-->
                            <div class="mix col-md-3 col-sm-6 seo margin-btm-20">
                                <a href="single-work.html">
                                    <div class="image-wrapper">
                                        <img src="{{asset('img/sec-img4.jpg')}}" class="img-responsive" alt="work-1">
                                        <div class="image-overlay">
                                            <p>
                                                View Detail
                                            </p>
                                        </div><!--.image-overlay-->
                                    </div><!--.image-wrapper-->
                                </a>
                                <div class="work-sesc">

                                    <p>
                                        Seo
                                    </p>
                                </div><!--.work-desc-->
                            </div><!--work item-->
                            <div class="mix col-md-3 col-sm-6 design margin-btm-20">
                                <a href="single-work.html">
                                    <div class="image-wrapper">
                                        <img src="{{asset('img/sec-img5.jpg')}}" class="img-responsive" alt="work-1">
                                        <div class="image-overlay">
                                            <p>
                                                View Detail
                                            </p>
                                        </div><!--.image-overlay-->
                                    </div><!--.image-wrapper-->
                                </a>
                                <div class="work-sesc">

                                    <p>
                                        Web design
                                    </p>
                                </div><!--.work-desc-->
                            </div><!--work item-->
                            <div class="mix col-md-3 col-sm-6 html seo margin-btm-20">
                                <a href="single-work.html">
                                    <div class="image-wrapper">
                                        <img src="{{asset('img/sec-img6.jpg')}}" class="img-responsive" alt="work-1">
                                        <div class="image-overlay">
                                            <p>
                                                View Detail
                                            </p>
                                        </div><!--.image-overlay-->
                                    </div><!--.image-wrapper-->
                                </a>
                                <div class="work-sesc">

                                    <p>
                                        Html/Seo
                                    </p>
                                </div><!--.work-desc-->
                            </div><!--work item-->
                            <div class="mix col-md-3 col-sm-6 design wordpress margin-btm-20">
                                <a href="single-work.html">
                                    <div class="image-wrapper">
                                        <img src="{{asset('img/sec-img7.jpg')}}" class="img-responsive" alt="work-1">
                                        <div class="image-overlay">
                                            <p>
                                                View Detail
                                            </p>
                                        </div><!--.image-overlay-->
                                    </div><!--.image-wrapper-->
                                </a>
                                <div class="work-sesc">

                                    <p>
                                        wordpress/web design
                                    </p>
                                </div><!--.work-desc-->
                            </div><!--work item-->
                            <div class="mix col-md-3 col-sm-6 html wordpress margin-btm-20">
                                <a href="single-work.html">
                                    <div class="image-wrapper">
                                        <img src="{{asset('img/sec-img8.jpg')}}" class="img-responsive" alt="work-1">
                                        <div class="image-overlay">
                                            <p>
                                                View Detail
                                            </p>
                                        </div><!--.image-overlay-->
                                    </div><!--.image-wrapper-->
                                </a>
                                <div class="work-sesc">

                                    <p>
                                        wordpress/Html
                                    </p>
                                </div><!--.work-desc-->
                            </div><!--work item-->
                        </div><!--#grid for filter items-->
                    </div><!--.col-md-12 of portfolio filter-->
                </div><!--.row-->
            </div><!--.contianer-->
        </div><!--work section 1-->
    </section><!--#work-section-->

<!--our work section end-->
