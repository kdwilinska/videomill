<!-- resources/views/auth/login.blade.php -->
@extends ('app')
@section('title')
    {{trans('auth.login')}}
@stop

@section('content')
<form method="POST" action="{{url($locale.'/auth/login')}}">
    {!! csrf_field() !!}

    <div>
        {{trans('auth.email')}}
        <input type="email" name="email" value="{{ old('email') }}">
    </div>

    <div>
        {{trans('auth.password')}}
        <input type="password" name="password" id="password">
    </div>

    <div>
        <input type="checkbox" name="remember"> {{trans('auth.remember_me')}}
    </div>

    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-lg btn-theme-color">{{trans('auth.login')}}</button>
    </div>


    <a href="{{url($locale.'/auth/register')}}">{{trans('auth.register')}}</a>

</form>

@include('errors.list')

@stop
