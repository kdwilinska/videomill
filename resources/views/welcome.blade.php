<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{trans('page.p_intro_slide_logo')}}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--bootstrap css-->
    <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!--custom css-->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
    <!--video player css css-->
    <link href="{{asset('css/YTPlayer.css')}}" rel="stylesheet" type="text/css">
    <!--flex slider css-->
    <link href="{{asset('css/flexslider.css')}}" rel="stylesheet">
    <!--google web fonts css-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <!-- icons css-->
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!--animated css-->
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <!--owl carousel css-->
    <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{asset('css/owl.theme.css')}}" rel="stylesheet" type="text/css" media="screen">
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->

</head>
    <body data-spy="scroll" data-target="#navigation" data-offset="80">
    <section id="home" data-stellar-background-ratio="0.5">
        <div class="parallax-overlay"></div>
        <a id="video" class="player" data-property="{videoURL:'http://youtu.be/R55e-uHQna0',containment:'#home', showControls:true, autoPlay:true, loop:true, mute:true, startAt:0, opacity:1, quality:'default'}"></a>

        <div class="home-content text-center">
            <div class="container">
                <h1 class=" slide-logo">{{trans('page.p_intro_slide_logo')}}</h1>
                <div class="main-flex-slider">
                    <ul class="slides">
                        <li>
                            <h1>{{trans('page.p_intro_slide_text_1')}}</h1>

                        </li>
                        <li>
                            <h1>{{trans('page.p_intro_slide_text_2')}}</h1>

                        </li>
                        <li>
                            <h1>{{trans('page.p_intro_slide_text_3')}}</h1>

                        </li>
                    </ul>
                </div>
                <h2 class="slide-btm-text">{{trans('page.p_intro_slide_btm_text')}}</h2>
                <div class="home-arrow-down text-center">
                    <p class="scrollto"><a href="#work" class="btn btn-lg btn-theme-color">{{trans('page.p_intro_slide_btn_start')}}</a></p>
                </div>
            </div>
        </div>
    </section>

    @include('include_menu', array ('page_type' => 'main'))

 <!--our work section start here-->
    <section id="work" class="padding-80">
        @include('include_work', array ('page_type' => 'main'))
        @include('include_testimonials', array ('page_type' => 'main'))
    </section><!--#work-section-->

    <section id="about" class="padding-80">
        @include('include_about', array ('page_type' => 'main'))

    </section>
    <!--about section end here-->
        @include('include_services', array ('page_type' => 'main'))
    <!--section services start here-->
    <section id="services" class="padding-80">

    </section>
    <!--section services end here-->
    <!--pricing table section start here-->
    <section id="pricing" class="padding-80">
        @include('include_pricing', array ('page_type' => 'main'))
    </section> <!--pricing table section end here-->

    <section id="contact" class="padding-80">
        @include('include_contact', array ('page_type' => 'main'))
    </section><!--contact section end-->

    <!--div id="map-canvas" style="width:100%; height: 350px;"></div-->
    @include('include_contact_small', array ('page_type' => 'main'))

    <section id="footer" class="padding-80">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 copyright">
                    <span>&copy;2014.yourCompany. All right reserved</span>
                </div>
                <div class="col-md-6 col-sm-6 footer-nav">
                    <ul class="list-inline">
                        <li><a href="#">{{trans('page.home_page')}}</a></li>

                    </ul>
                </div>
            </div>
        </div>
    </section><!--footer end-->
    <!--back to top-->
    <a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
    <!--back to top end-->




    <!--script files-->
    <script src="{{asset('js/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.easing.1.3.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.mixitup.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.mb.YTPlayer.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.flexslider-min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/wow.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.sticky.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.stellar.min.js')}}" type="text/javascript"></script>
    <!--new version v1.1 plugins-->
    <script src="{{asset('js/owl.carousel.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.nicescroll.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/waypoints.min.js')}}"></script>
    <script src="{{asset('js/easypiechart.js')}}"></script>
    <script src="{{asset('js/jquery.counterup.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.countdown.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/contact_me.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jqBootstrapValidation.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/custom.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript">
        var myLatlng;
        var map;
        var marker;

        function initialize() {
            myLatlng = new google.maps.LatLng(37.397802, -121.890288);

            var mapOptions = {
                zoom: 13,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                draggable: false
            };
            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            var contentString = '<p style="line-height: 20px;"><strong>Banzhow Template</strong></p><p>123 My Street, Banzhow City, CA 4567</p>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Marker'
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, marker);
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    </body>
</html>
