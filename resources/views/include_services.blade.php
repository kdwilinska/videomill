
<div class="services-section-1">
    <div class="container">
        <div class="section-heading text-center">
            <h4 class="small section-title"><span>{{trans('page.s_tittle_small')}}</span></h4>
            <h2 class="large section-title">{{trans('page.s_services')}}</h2>
        </div><!--section heading-->

    </div><!--container-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="margin-btm-40">
                    <h4>{{trans('page.s_tittle')}}</h4>
                    <p>
                        {{trans('page.s_description')}}
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 wow animated fadeInLeft" data-wow-delay="0.3s">

                <div class="row margin-btm-20">
                    <div class="col-md-2">
                        <div class="services-icon">
                            <i class="fa fa-image"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="services-info">
                            <h3>{{trans('page.s_1l_name')}}</h3>
                            <p>
                                {{trans('page.s_1l_description')}}
                            </p>
                        </div>
                    </div>
                </div><!--.services row end-->
                <div class="row margin-btm-20">
                    <div class="col-md-2">
                        <div class="services-icon">
                            <i class="fa fa-plane"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="services-info">
                            <h3>{{trans('page.s_2l_name')}}</h3>
                            <p>
                                {{trans('page.s_2l_description')}}
                            </p>
                        </div>
                    </div>
                </div><!--.services row end-->
                <div class="row margin-btm-20">
                    <div class="col-md-2">
                        <div class="services-icon">
                            <i class="fa fa-leaf"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="services-info">
                            <h3>{{trans('page.s_3l_name')}}</h3>
                            <p>
                                {{trans('page.s_3l_description')}}
                            </p>
                        </div>
                    </div>
                </div><!--.services row end-->
                <div class="row margin-btm-20">
                    <div class="col-md-2">
                        <div class="services-icon">
                            <i class="fa fa-paper-plane"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="services-info">
                            <h3>{{trans('page.s_4l_name')}}</h3>
                            <p>
                                {{trans('page.s_4l_description')}}
                            </p>
                        </div>
                    </div>
                </div><!--.services row end-->
            </div>
            <div class="col-md-6 wow animated fadeInRight" data-wow-delay="0.6s">

                <div class="row margin-btm-20">
                    <div class="col-md-2">
                        <div class="services-icon">
                            <i class="fa fa-image"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="services-info">
                            <h3>{{trans('page.s_1r_name')}}</h3>
                            <p>
                                {{trans('page.s_1r_description')}}
                            </p>
                        </div>
                    </div>
                </div><!--.services row end-->
                <div class="row margin-btm-20">
                    <div class="col-md-2">
                        <div class="services-icon">
                            <i class="fa fa-plane"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="services-info">
                            <h3>{{trans('page.s_2r_name')}}</h3>
                            <p>
                                {{trans('page.s_2r_description')}}
                            </p>
                        </div>
                    </div>
                </div><!--.services row end-->
                <div class="row margin-btm-20">
                    <div class="col-md-2">
                        <div class="services-icon">
                            <i class="fa fa-leaf"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="services-info">
                            <h3>{{trans('page.s_3r_name')}}</h3>
                            <p>
                                {{trans('page.s_3r_description')}}
                            </p>
                        </div>
                    </div>
                </div><!--.services row end-->
                <div class="row margin-btm-20">
                    <div class="col-md-2">
                        <div class="services-icon">
                            <i class="fa fa-paper-plane"></i>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="services-info">
                            <h3>{{trans('page.s_4r_name')}}</h3>
                            <p>
                                {{trans('page.s_4r_description')}}
                            </p>
                        </div>
                    </div>
                </div><!--.services row end-->
            </div>
        </div>
    </div>
</div><!--services section 1 end-->
<div class="services-section-2 parallax" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>{{trans('page.s_subscribe_title')}}</h1>
                <form class="newsletter-form">
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="input-group">
                                <label class="sr-only" for="subscribe-email">{{trans('page.s_subscribe_email_label')}}</label>
                                <input type="email" class="form-control" id="subscribe-email" placeholder="{{trans('page.s_subscribe_email_hint')}}">
                                            <span class="input-group-btn">
                                                <button type="submit" class="btn btn-theme-color btn-lg">{{trans('page.s_subscribe_button')}}</button>
                                            </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!--services section 1 end-->
