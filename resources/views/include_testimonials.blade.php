
        <!--testimonials-->
        <div class="testi parallax" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <div id="testi-carousel" class="owl-carousel">
                            <div>
                                <h4>
                                    {{trans('page.testimonials1')}}
                                </h4>
                                <p>{{trans('page.testimonials1_author')}}</p>
                            </div><!--testimonials item like paragraph-->
                            <div>
                                <h4>
                                    {{trans('page.testimonials2')}}
                                </h4>
                                <p>{{trans('page.testimonials2_author')}}</p>
                            </div><!--testimonials item like paragraph-->
                            <div>
                                <h4>
                                    {{trans('page.testimonials3')}}
                                </h4>
                                <p>{{trans('page.testimonials3_author')}}</p>
                            </div><!--testimonials item like paragraph-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--testimonials-->
