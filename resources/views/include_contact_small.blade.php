<div class="contact-sec-2">
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-4">
                <div class="contact-col wow animated flipInY" data-wow-delay=".3s">
                    <i class="fa fa-phone"></i>
                    <p>+48 88 555 1 555</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="contact-col wow animated flipInY" data-wow-delay=".3s">
                    <i class="fa fa-envelope"></i>
                    <p>info@videomill.pl</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="contact-col wow animated flipInY" data-wow-delay=".3s">
                    <i class="fa fa-home"></i>
                    <p>{{trans('page.c_info_address_line1')}}<br>{{trans('page.c_info_address_line2')}}</p>
                </div>
            </div>
        </div>
    </div>
</div><!--Contact-sec-2 end-->
