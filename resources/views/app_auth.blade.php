<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--bootstrap css-->
    <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!--custom css-->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
    <!--flex slider css-->
    <link href="{{asset('css/flexslider.css')}}" rel="stylesheet">
    <!--google web fonts css-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <!-- icons css-->
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body data-spy="scroll" data-target="#navigation" data-offset="75">


@include('include_menu')

//{$locale}}

<!--section id="page-head-bg">
    <div class="container">
        <h1>Blog post</h1>
    </div>
</section--><!--page-head bg end-->

<!--section blog post start here-->
@yield('content')

<section id="blog-post" class="padding-80">
    <div class="container">

            <div class="col-md-9">
                <div class="blog-item-sec">
                    <!--blog -post comment wrapper start-->
                    <div class="comment-wrapper">
                        <div class="comment-box">
                      <div class="comment-form">
                            <h4>Leave a Comment</h4>
                            <form class="wow animated fadeInUp">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Name" id="name">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="email" class="form-control" placeholder="Email" id="password">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Subject" id="subject">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Company" id="company">
                                    </div>
                                    <div class="col-md-12">
                                        <textarea class="form-control" placeholder="Your Comment" rows="7"></textarea>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-lg btn-theme-color">Send Comment</button>
                                    </div>
                                </div>
                            </form>
                        </div>



                    </div>


                    <!--blog post comment wrapper end-->
                </div>  <!--blog-item section end-->
            </div>


    </div>
</section>
<!--section blog post end here-->
<section id="footer" class="padding-50">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 copyright">
                <span>&copy;2014.yourCompany. All right reserved</span>
            </div>
            <div class="col-md-6 col-sm-6 footer-nav">
                <ul class="list-inline">
                    <li><a href="index.html">Home</a></li>
                </ul>
            </div>
        </div>
    </div>
</section><!--footer end-->
<!--back to top-->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--back to top end-->
<!--script files-->
<script src="{{asset('js/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.easing.1.3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.mixitup.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.mb.YTPlayer.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.flexslider-min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/wow.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.sticky.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.stellar.min.js')}}" type="text/javascript"></script>
<!--new version v1.1 plugins-->
<script src="{{asset('js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.nicescroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/waypoints.min.js')}}"></script>
<script src="{{asset('js/easypiechart.js')}}"></script>
<script src="{{asset('js/jquery.counterup.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.countdown.js')}}" type="text/javascript"></script>
<script src="{{asset('js/contact_me.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jqBootstrapValidation.js')}}" type="text/javascript"></script>
<script src="{{asset('js/custom.js')}}" type="text/javascript"></script>
</body>
</html>


