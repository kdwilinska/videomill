<div class="contact-sec-1">
    <div class="container">
        <div class="section-heading text-center">
            <h4 class="small section-title"><span>{{trans('page.c_title_small')}} </span></h4>
            <h2 class="large section-title">{{trans('page.c_title')}}</h2>
        </div><!--section heading-->
    </div><!--.container-->
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h4>{{trans('page.c_form_title')}}</h4>
                <form name="sentMessage" id="contactForm" method="post" novalidate>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row control-group">
                                <div class="form-group col-xs-12 controls">
                                    <label>{{trans('page.c_form_name')}}<span>*</span></label>
                                    <input type="text" class="form-control" placeholder="Name" id="name" required data-validation-required-message="{{trans('page.c_form_name_err_required')}}">
                                    <p class="help-block"></p>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="row control-group">
                                <div class="form-group col-xs-12 controls">
                                    <label>{{trans('page.c_form_email')}}<span>*</span></label>
                                    <input type="email" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="{{trans('page.c_form_email_err_required')}}">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 controls">
                            <label>{{trans('page.c_form_message')}}<span>*</span></label>
                            <textarea rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="{{trans('page.c_form_message_err_required')}}"></textarea>
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <br>
                    <div id="success"></div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <button type="submit" class="btn btn-theme-color btn-lg">{{trans('page.c_form_submit')}}</button>
                        </div>
                    </div>
                </form>
                <!--contact form-->
            </div>
            <div class="col-md-4">
                <h4>{{trans('page.c_info_title')}}</h4>
                <div class="contact-info wow animated fadeInRight" data-wow-delay=".6s">
                    <p><i class="fa fa-home"></i>{{trans('page.c_info_address_line1')}} {{trans('page.c_info_address_line2')}}</p>
                    <p><i class="fa fa-home"></i>+48 88 555 1 555</p>
                    <p><i class="fa fa-envelope"></i> <a href="#">info@videomill.pl</a></p>
                    <p><i class="fa fa-clock-o"></i>{{trans('page.c_info_opening')}}</p>
                    <hr>
                    <h4>Elsewhere</h4>
                    <ul class="list-inline social-colored">
                        <li><a href="#"><i class="fa fa-facebook icon-fb" data-toggle="tooltip" title="" data-original-title="Facebook" data-placement="top"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter icon-twit" data-toggle="tooltip" title="" data-original-title="Twitter" data-placement="top"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus icon-plus" data-toggle="tooltip" title="" data-original-title="Google pluse" data-placement="top"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin icon-in" data-toggle="tooltip" title="" data-original-title="Linkedin" data-placement="top"></i></a></li>

                    </ul> <!--colored social-->
                </div>
            </div>
        </div>
    </div>
</div><!--Contact-sec-1 end-->
