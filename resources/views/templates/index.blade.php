@extends('app')

@section('title')
    Templates
@stop

@section('content')
    <h1>{{trans('page.templates')}}</h1>

    @foreach($templates as $template)

        <!--template-->
            <h2>
                <a href="{{url('/templates',$template->id)}}">{{$template->title}}</a>
            </h2>
            <div class="body">{{$template->body }}</div>

        @if (count($template->tags))
            <div class="tag_list">Tags:
                @foreach($template->tags as $tag)
                    {{$tag->name}}
                @endforeach
            </div>

        @endif


                <!--/template-->
    @endforeach
@stop