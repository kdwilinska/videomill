@extends('app')
@section('title')
    Create template
@stop
@section('content')
    <h1>Create a New Template</h1>



    {!! Form::open(['url' => $locale.'/templates']) !!}
        @include('templates.form',['submitButtonText'=>'Ad Template'])

    {!! Form::close() !!}

    @include('errors.list')

@stop