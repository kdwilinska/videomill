@extends('app')
@section('title')
    {{$template->title}}
@stop
@section('content')

    <a href="{{url('/templates')}}">{{trans('page.templates')}}</a>

    <h1>{{$template->title}}</h1>

{{$template->body }}

    @if (count($template->tags))
        <h3>Tags:</h3>
        <ul>
            @foreach($template->tags as $tag)
                <li>{{$tag->name}}</li>
            @endforeach
        </ul>

    @endif


    <a href="{{url( '/templates/'.$template->id .'/edit')}}">Edit template</a>

@stop