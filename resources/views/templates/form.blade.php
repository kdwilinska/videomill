

<div class="form-group">
    {!! Form::label('title','Title:') !!}
    {!! Form::text('title',null,['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('body','Body:') !!}
    {!! Form::textarea('body',null,['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('published_at','Publish On:') !!}
    {!! Form::input('date','published_at',date('Y-m-d'),['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('tags','Tags:') !!}
    {!! Form::select('tags[]',$tags,$chosenTags,['class' => 'form-control','multiple']) !!}
</div>


<div class="form-group">
    <button type="submit" class="btn btn-theme-color btn-lg">{{$submitButtonText}}</button>
    !! Form::submit($submitButtonText,null,['class' => 'btn btn-theme-color btn-lg']) !!}
</div>
