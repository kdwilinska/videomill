@extends('app')
@section('title')
    Edit template
@stop
@section('content')
    <h1>Edit Template {!! $template->title !!}</h1>



    {!! Form::model($template,['method'=>'PATCH','action' => ['TemplatesController@update',$template->id]]) !!}
        @include('templates.form',['submitButtonText'=>'Save Template'])
    {!! Form::close() !!}

 @include('errors.list')

@stop