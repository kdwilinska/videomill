<section id="navigation">
    <div class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{asset('/')}}">{{trans('page.p_intro_slide_logo')}}</a>
            </div>
            <div class="navbar-collapse collapse">



                <ul class="nav navbar-nav navbar-right scrollto">
                    @if ($page_type=='main')
                        <li><a href="#home">{{trans('page.home_page')}}</a></li>
                    @else
                        <li><a href="{{asset('/'.$locale.'/#home')}}">{{trans('page.home_page')}}</a></li>
                    @endif
                        <li><a href="{{url($locale.'/video/create')}}">{{trans('page.v_create_video')}}</a></li>
                    @if (Auth::check())
                            @if ($page_type=='main')
                                <li><a href="#video">{{trans('page.v_my_videos')}}</a></li>
                            @else
                                <li><a href="{{asset('/'.$locale.'/#video')}}">{{trans('page.v_my_videos')}}</a></li>
                            @endif
                    @endif
                        @if ($page_type=='main')
                            <li><a href="#work">{{trans('page.t_templates')}}</a></li>
                        @else
                            <li><a href="{{asset('/'.$locale.'/#work')}}">{{trans('page.t_templates')}}</a></li>
                        @endif

                        @if ($page_type=='main')
                            <li><a href="#about">{{trans('page.a_about')}}</a></li>
                        @else
                            <li><a href="{{asset('/'.$locale.'/#about')}}">{{trans('page.a_about')}}</a></li>
                        @endif

                        @if ($page_type=='main')
                            <li><a href="#contact">{{trans('page.c_contact')}}</a></li>
                        @else
                            <li><a href="{{asset('/'.$locale.'/#contact')}}">{{trans('page.c_contact')}}</a></li>
                        @endif

                    @if (Auth::check())
                        {!! Auth::user()->first_name !!}
                        <li><a href="{{url($locale.'/my-account')}}">{{trans('page.u_my_account')}}</a></li>
                        <li><a href="{{url($locale.'/auth/logout')}}">{{trans('auth.logout')}}</a></li>
                    @else
                        <li><a href="{{url($locale.'/auth/login')}}">{{trans('auth.login')}}</a></li>
                        <li><a href="{{url($locale.'/auth/register')}}">{{trans('auth.register')}}</a></li>
                     @endif


                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle js-activated">{{$locale}}<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/en/#home">EN</a></li>
                            <li><a href="/pl/#home">PL</a></li>
                        </ul>
                    </li>

                </ul>

            </div><!--/.nav-collapse -->
        </div><!--/.container -->
    </div>
</section><!--navigation section end here-->
