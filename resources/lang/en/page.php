<?php
return [

    'p_intro_slide_logo' => 'Videomill',
    'p_intro_slide_text_1' => 'Personalised Video',
    'p_intro_slide_text_2' => 'Postproduction',
    'p_intro_slide_text_3' => 'Videomailing, sms and display',
    'p_intro_slide_btm_text' => 'higher level of communication and targeting',
    'p_intro_slide_btn_start' => 'More',


    'home_page' => 'Home Page',
    't_templates' => 'Templates',

    'v_my_videos' => 'My videos',
    'v_create_video' => 'Create video',


    'a_about' => 'About',
    'a_' => '',
    /*'a_' => 'Tworzymy Wideo',
    'a_' => 'Nie jedno wideo na milion, a milion wideo na milion użytkowników.',
    'a_' => 'Big Idea dla Big Data',
    'a_' => 'Czyli opracowanie koncepcji ugryzienia tematu. Wielowątkowo patrząc na potrzeby klientów, kanały dotarcia i składowe oferty.',
    'a_' => 'Scenariusz ze zmiennymi pod personalizację',
    'a_' => 'Przekucie w historię sprzedażową tego co wiemy, i konsument chciałby wiedzieć.',
    'a_' => 'Analiza danych klientów',
    'a_' => 'Oddzielenie ziaren od plew i wyciągnięcie najbardziej konwertujących wniosków.',
    'a_' => 'Testowanie i targetowanie',
    'a_' => 'Oszacowanie sprawności działania, jego dróg, i obranie najwłaściwszych opcji kampanii końcowej.',
    'a_description' => 'Wykorzystujemy innowacyjną technologię personalizacji filmów wideo. Całość działań prowadzimy inhouse w oparciu o własne rozwiązania technologiczne oraz doświadczenie w tworzeniu kreatywnych i niestandardowych form komunikacji z konsumentami. Od produkcji w studio filmowym, po multiplikację (powielanie) filmów na własnej platformie renderującej. Po drodze opracowując koncepcję, wizualizacje i przechodząc pozostałe etapy od briefu do statystyk konwersji kampanii.',
   */

    'c_contact' => 'Contact',
    'c_title_small' => 'we Love to here from you',
    'c_title' => 'Contact Us',
    'c_form_title' => 'Get in touch',
    'c_form_name' => 'Name',
    'c_form_name_err_required' => 'Please enter your name.',
    'c_form_email' => 'Email Address',
    'c_form_email_err_required' => 'Please enter your email address.',
    'c_form_message' => 'Message',
    'c_form_message_err_required' => 'Please enter a message.',
    'c_form_submit' => 'Send Message',
    'c_info_title' => 'Contact info',
    'c_info_address_line0' => 'VideoMill Ltd.',
    'c_info_address_line1' => '465 Puławska Street',
    'c_info_address_line2' => '02-884 Warsaw, Poland',
    'c_info_opening' => 'Monday-Friday 9:00-5:00pm',
    'c_info_phone' => '+48 88 555 1 555',
    'c_info_email' => 'info@videomill.pl',

    's_services' => 'Our Services',
    's_tittle_small' => 'What we do?',
    's_tittle' => 'Personalized Video and more..',
    's_description' => 'Personalized Video - it\'s cool',
    's_1l_name' => 'Video',
    's_1l_description' => 'People likes video :)',
    's_2l_name' => 'Personalisation',
    's_2l_description' => 'Everybody likes hear your name..  ',
    's_3l_name' => 'Production',
    's_3l_description' => 'Let\'s make it!',
    's_4l_name' => 'Animation',
    's_4l_description' => 'This is fun!',
    's_1r_name' => 'Post-production',
    's_1r_description' => 'This is hard work..',
    's_2r_name' => 'Mailing',
    's_2r_description' => 'Send me a letter',
    's_3r_name' => 'Video-mailing',
    's_3r_description' => 'Show me your video',
    's_4r_name' => 'Targeting',
    's_4r_description' => 'This is for you',
    's_subscribe_title' => 'Subscribe to Newsletter',
    's_subscribe_email_label' => 'Email address',
    's_subscribe_email_hint' => 'Enter your email',
    's_subscribe_button' => 'OK',




    'testimonials1'=>'Watching the video is much easier than reading the text. Your mind will 60,000 times faster understand what you watched what you read.',
    'testimonials1_author'=>'We combine and unite the most effective forms of consumer dialogue: video, personal data, and interaction.',
    'testimonials2'=>'It is a very lucid medium and thanks to that the advertising message is easily remembered.',
    'testimonials2_author'=>'Video marketing',
    'testimonials3'=>'We efficiently draw the customer’s attention. Based on behavioural information, we customise the promoted offer to meet the consumer’s needs and expectations.',
    'testimonials3_author'=>'Personal data',

    'u_my_account' => 'My account',
    'u_submit_save_user'=>'Save user',
    'u_email'=>'E-mail',
    'u_first_name'=>'First name',
    'u_last_name'=>'Last name',
    'u_phone'=>'Phone',
    'u_company_name'=>'Company name',
    'u_company_tax_number'=>'Tax number',
    'u_company_country'=>'Country',
    'u_address_line_1'=>'Address',
    'u_address_line_2'=>'',
    'u_address_postal_code'=>'Postal code',
    'u_address_post_office'=>'Post office',
    'u_account_data_label'=>'Your account',
    'u_company_data_label'=>'Billing information',



];