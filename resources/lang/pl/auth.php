<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Podano nieprawidłowe dane do logowania. Proszę spróbować ponownie.',
    'throttle' => 'Zbyt wiele prób logowania. Proszę spróbować ponownie za :seconds sekund.',
    'email' => 'e-mail',
    'password' => 'hasło',
    'remember_me' => 'pamiętaj mnie',
    'login' => 'zaloguj',
    'logout' => 'wyloguj',
    'register' => 'zarejestruj',

];
