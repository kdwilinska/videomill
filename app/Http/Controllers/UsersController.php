<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Tag;
use App\Template;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;


class UsersController extends Controller
{

    public function show($locale = 'en')
    {

        \App::setLocale($locale);

        if (Auth::check())
        {
            $curr_user_id=Auth::user()->id;
            $curr_user=User::whereId($curr_user_id)->first();
            //dd($user);
            return view('users.show',compact('curr_user','locale'));
        }
        else
        {
            return redirect( $locale.'/auth/login');
        }
    }

    public function update_curr_user($locale = 'en', Requests\UserRequest $request)
    {
        \App::setLocale($locale);

        $curr_user_id=Auth::user()->id;
        $curr_user=User::whereId($curr_user_id)->first();

        //if (is_null($template)){ abort(404);}

        $curr_user->update($request->all());
        // $template->tags()->detach();
        //  $template->tags()->attach( $request->input('tags'));
        //$template->tags()->sync( $request->input('tags'));


        return redirect($locale.'/my-account');

    }


    public function edit($locale = 'en')
    {

        \App::setLocale($locale);

        if (Auth::check())
        {
            $curr_user_id=Auth::user()->id;
            $curr_user=User::whereId($curr_user_id)->first();
            //dd($user);
            return view('users.edit',compact('curr_user','locale'));
        }
        else
        {
            return redirect( $locale.'/auth/login');
        }
    }


}
