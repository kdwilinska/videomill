<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Tag;
use App\Template;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;


class TemplatesController extends Controller
{
    public function index($locale = 'en')
    {
        \App::setLocale($locale);
        //dd($locale);
       // $templates=Template::latest('published_at')->get();
      //
      //  $templates=Template::where('title','like', 'k%')->orWhere('title', 'like', 't%')->get();

        $templates=Template::latest('published_at')->get();

       // dd($templates);
        return view('templates.index',compact('templates','locale'));
    }

    public function show($id,$locale = 'en')
    {

        \App::setLocale($locale);
        $template=Template::whereId($id)->with('tags')->first();

        if (is_null($template))
        {
            abort(404);
        }
        //dd($template);
        return view('templates.show',compact('template','locale'));

        //return $template;
    }

    public function create($locale = 'en')
    {
        \App::setLocale($locale);
        $tags = Tag::lists('name','id');
        $chosenTags = [];

        return view('templates.create',compact('tags','chosenTags','locale'));
    }

    public function store(Requests\TemplateRequest $request,$locale = 'en')
    {

        //$request = $request->all();
        //$request['published_at'] = Carbon::now();
        //$request['user_id'] = Auth::user()->id;


       // Template::create($request->all());

        //dd($request->input('tags'));
        \App::setLocale($locale);

        $template = new Template($request->all());


        Auth::user()->templates()->save($template);


        $template->tags()->attach( $request->input('tags'));

        return redirect( $locale.'/templates');

    }

    public function edit($id)
    {
        //$template=Template::find($id);//where('id','=',$id);//->with('tags')->get();
        $template=Template::whereId($id)->with('tags')->first();

        $tags = Tag::lists('name','id');

        $chosenTags = $template->tags->pluck('id')->toArray();

        //dd($chosenTags);
        if (is_null($template))
        {
            abort(404);
        }
        return view('templates.edit',compact('template','tags', 'chosenTags'));


    }


    public function update($id, Requests\TemplateRequest $request)
    {
        $template=Template::findOrFail($id);

        //if (is_null($template)){ abort(404);}

        $template->update($request->all());
       // $template->tags()->detach();
      //  $template->tags()->attach( $request->input('tags'));
        $template->tags()->sync( $request->input('tags'));

        return redirect('templates');

    }

}
