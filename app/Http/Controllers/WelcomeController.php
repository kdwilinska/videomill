<?php


namespace App\Http\Controllers;

use App\Template;

class WelcomeController extends Controller {

    public function index($locale = 'en')
    {

        \App::setLocale($locale);
        //return 'Hello Word';
       // $templates=Template::latest('published_at')->get();

       //return view ('welcome',compact('templates'));
        return view ('welcome',compact('locale'));
    }



    public function contact($locale = 'en')
    {
       // return 'contact me';
        return view ('pages/contact');
    }


}

