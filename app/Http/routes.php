<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {   return view('welcome');

Route::get('{locale?}/','WelcomeController@index')
        ->where('locale', 'pl|en');


Route::get('/{locale?}/my-account/','UsersController@edit');
Route::post('/{locale?}/my-account/','UsersController@update_curr_user');

Route::get('/{locale?}/video/create','WelcomeController@index');


//Route::get('/templates','TemplatesController@index');
Route::get('/{locale?}/templates','TemplatesController@index');
Route::get('{locale?}/templates/create/','TemplatesController@create');
Route::get('{locale?}//templates/{id}/edit','TemplatesController@edit');
Route::get('{locale?}/templates/{id}','TemplatesController@show');
Route::post('{locale?}/templates','TemplatesController@store');
Route::post('templates','TemplatesController@store');



Route::controllers([
    'auth'=>'Auth\AuthController',
    '{locale?}/auth'=>'Auth\AuthController',
    'password'=>'Auth\PasswordController'
]);
