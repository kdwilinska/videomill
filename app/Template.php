<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    static protected $plec = [
        'K',
        'M'
    ];


    protected $fillable = [
        'title',
        'body',
        'published_at',
        'user_id' // temporary!!
    ];

    protected $dates = ['published_at'];

    public function scopePublished($query)
    {
        $query->where ('published_at','<=',Carbon::now());
    }

    public function stePublishedAtAttribute($date)
    {
        $this->attributes['published_at'] = Carbon::parse($date);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

  //  public function getTagListAttribute()
  //  {
  //      return $this->tags->lists('id');
  //  }

}
